/*
 * app.js -- WebSockets server for CTM Radio
 *
 * Copyright 2017 - 2020 Felipe Peñailillo <breadmaker@ctmradio.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

const config = require('./config');
const fs = require('fs');
const https = require('https');
const WebSocket = require('ws');

const server = https.createServer({
  cert: fs.readFileSync(config.certFile),
  key: fs.readFileSync(config.privkeyFile)
});
const wss = new WebSocket.Server({
  server
});

wss.on('connection', function connection(ws) {
  let jsonfile = config.jsonFile;
  let jsonContents = JSON.stringify(JSON.parse(fs.readFileSync(jsonfile)));
  let pingTimer = setInterval(function() {
    ws.send('ping');
  }, 60000);
  ws.send(jsonContents);
  fs.watch(jsonfile, function() {
    let currentContents = fs.readFileSync(jsonfile);
    if (currentContents.length > 0 && jsonContents !== JSON.stringify(JSON.parse(currentContents))) {
      jsonContents = JSON.stringify(JSON.parse(currentContents));
      clearInterval(pingTimer);
      pingTimer = setInterval(function() {
        ws.send('ping');
      }, 60000);
      ws.send(jsonContents);
    }
  });
});

server.listen(config.port);
