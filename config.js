/*
 * config.js -- Configuration variables for the WebSockets server
 *
 * Copyright 2017 - 2020 Felipe Peñailillo <breadmaker@ctmradio.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

module.exports = {
  jsonFile: '/path/to/now_playing.json', // Path to mutable now playing info file (provided by Liquidsoap)
  certFile: '/path/to/cert.pem', // Path to cert file (SCRIPT MUST HAVE READ ACCESS)
  privkeyFile: '/path/to/privkey.pem', // Path to privkey file (SCRIPT MUST HAVE READ ACCESS)
  port: 7777 // WebSockets exposed port, be careful to not double assign a port
};
