# WebSockets server for CTM Radio

This [node.js](https://nodejs.org/) simple script implements a WebSockets
protocol server that returns the following JSON data:

```json
{ "title": "Títle", "artist": "Artist" }
```

Which is the minimal metadata required to make a call to the
[API](https://gitlab.com/ctm-radio/rctm-api). This project will serve as a base
to a next iteration of such API using WebSockets.

## How to connect

Using any modern browser will do, because all latest versions support the
WebSockets protocol:

```javascript
var ctmws = new WebSocket("wss://ws.ctmradio.org");
ctmws.onmessage = function(event) {
  if (event.data !== "ping") {
    console.log('New metadata received: ' + event.data);
  } else {
    console.log('Ping received');
  }
}
// TODO: RECONNECT ON ERROR
```

## How does it work

:warning: Technical details below.

The server keeps running an instance of [`ws`](https://www.npmjs.com/package/ws)
thanks to [`forever`](https://www.npmjs.com/package/forever). This server
listens to mutations of a file defined under `jsonFile` in the `config.js` file,
which contains the minimal metadata of the song currently played bt the
Liquidsoap server, and that file is updated by the LS script every time a new
song is played.
Every time the file mutates, it checks for duplicate information, because
depending of the configuration of the Liquidsoap server, it may fire more than
one `on_metadata` event. If the latest mutation contains new data, a new
WebSocket broadcast containing such data is emitted.

The server is SSL by default, so remember to indicate the paths to the pem files
in the `config.js` file.

Lastly, don't forget you can also change the server port in such config file.

## How to use

Just install and run:

```bash
$ cd path/to/rctm-ws
$ npm install
$ npm run start
```

You can then `npm run stop` or `npm run restart` at will.
